# test-proto

 Create CSharp projects

 ```
 dotnet new classlib
 ```   
 
Open csproj file, add this section to `PropertyGroup`:
```
<PackageId>Test</PackageId>
<Version>1.0.0</Version>
<Authors>your_name</Authors>
<Company>your_company</Company>
```

Gen proto

```shell
protoc --csharp_out=src api/hello.proto
```
Build nuget package

```shell
dotnet pack -c Release
```

Add nuget gitlab source and publish
```shell
dotnet nuget add source "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/nuget/index.json" --name gitlab --username gitlab-ci-token --password $CI_JOB_TOKEN --store-password-in-clear-text


dotnet nuget push "bin/Release/*.nupkg" --source gitlab
```



Docs:

https://docs.microsoft.com/en-us/nuget/quickstart/create-and-publish-a-package-using-the-dotnet-cli

https://docs.gitlab.com/ee/user/packages/nuget_repository/#publish-a-nuget-package